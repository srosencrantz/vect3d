/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package vect3d

import (
	"math"
	"testing"
)

func TestDotProduct(t *testing.T) {

	a := [3]float64{3, 1, -1}
	b := [3]float64{0, 2, 3}
	ans := DotProduct(a, b)
	if ans != float64(-1.0) {
		t.Errorf("expected -1 got %10.6f", ans)
	}
}
func TestDistPt2Line(t *testing.T) {

	a := [3]float64{3, 1, -1}
	b := [3]float64{0, 2, 3}
	n := [3]float64{2, 1, 2}
	ans := DistPt2Line(a, n, b)
	if ans != float64(5.0) {
		t.Errorf("expected 5.0 got %10.6f", ans)
	}
}
func TestDistPt2Pt(t *testing.T) {

	a := [3]float64{0, 0, 0}
	b := [3]float64{1, 1, 1}
	ans := DistPt2Pt(a, b)
	if ans != float64(1.732050807568877293) {
		t.Errorf("expected 1.73205 got %10.6f", ans)
	}

}
func TestCrossProduct(t *testing.T) {

	a := [3]float64{1, 0, 0}
	b := [3]float64{1, 1, 1}
	c := [3]float64{0, -1, 1}
	ans := CrossProduct(a, b)
	if ans != c {
		t.Errorf("expected 0,-1,1 got %v", ans)
	}

}

func Close(n1, n2 [3]float64, tol float64) bool {
	diff := Magnitude(Subtract(n1, n2))
	return diff < tol
}

func TestRotateX(t *testing.T) {
	test := [3]float64{10, 9, 8}
	angle := (float64(23) * math.Pi) / float64(180.0)
	center := [3]float64{-1, -2, -3}
	result := [3]float64{10.0, 3.8275108, 11.423595}
	test = RotateX(test, center, angle)

	if !Close(test, result, 1.0E-4) {
		t.Errorf("RotateX result doesn't match expected, expected:\n%v\nGot:\n%v\n", result, test)
	}

}

func TestRotateXa(t *testing.T) {
	test := [3]float64{0, 1, 0}
	angle := float64(math.Pi / float64(2.0))
	center := [3]float64{0, 0, 0}
	result := [3]float64{0, 0, 1}
	test = RotateX(test, center, angle)

	if !Close(test, result, 1.0E-4) {
		t.Errorf("RotateX result doesn't match expected, expected:\n%v\nGot:\n%v\n", result, test)
	}

}

func TestRotateY(t *testing.T) {
	test := [3]float64{10, 9, 8}
	angle := (float64(91) * math.Pi) / float64(180.0)
	center := [3]float64{-4, -5, -6}
	result := [3]float64{9.7535343, 9.0, -20.242201}
	test = RotateY(test, center, angle)

	if !Close(test, result, 1.0E-4) {
		t.Errorf("RotateY result doesn't match expected, expected:\n%v\nGot:\n%v\n", result, test)
	}
}

func TestRotateZ(t *testing.T) {
	test := [3]float64{10, 9, 8}
	angle := (float64(-270) * math.Pi) / float64(180.0)
	center := [3]float64{33, -34, 35}
	result := [3]float64{-9.9999962, -57.000008, 8.0}
	test = RotateZ(test, center, angle)

	if !Close(test, result, 1.0E-4) {
		t.Errorf("RotateZ result doesn't match expected, expected:\n%v\nGot:\n%v\n", result, test)
	}
}

func TestRotateN1N2(t *testing.T) {
	// this tests RotateN1N2 and RotateAxis
	test := [3]float64{10, 9, 8}
	angle := (float64(333) * math.Pi) / float64(180.0)
	center := [3]float64{1.5, 2.8, 3.2}
	n1 := [3]float64{50, 10, 3}
	n2 := [3]float64{3, 10, 50}
	result := [3]float64{11.265516, 4.0546761, 9.2655163}
	test = RotateN1N2(test, center, n1, n2, angle)

	if !Close(test, result, 1.0E-4) {
		t.Errorf("RotateN1N2 result doesn't match expected, expected:\n%v\nGot:\n%v\n", result, test)
	}
}

func TestRotateN1N2N3(t *testing.T) {
	point := [3]float64{10, 9, 8}
	angle := (float64(13) * math.Pi) / float64(180.0)
	origin := [3]float64{1.2, 3.4, 5.6}
	n1 := [3]float64{10, 9, 8}
	n2 := [3]float64{11, 12, 13}
	n3 := [3]float64{14, 15, 16}
	result := [3]float64{8.8193636, 9.4442215, 10.069080}
	test, err := RotateN1N2N3(point, origin, n1, n2, n3, angle)

	if err != nil {
		t.Error(err)
	}

	if !Close(test, result, 1.0E-4) {
		t.Errorf("RotateN1N2N3 result doesn't match expected, expected:\n%v\nGot:\n%v\n", result, test)
	}
}

func TestYawPitchRoll(t *testing.T) {
	point1 := [3]float64{1.0, 0.0, 0.0}
	point2 := [3]float64{0.0, 1.0, 0.0}
	point3 := [3]float64{0.0, 0.0, 1.0}

	yaw := 90.0
	pitch := 45.0
	roll := 90.0
	expectedResult1 := [3]float64{0.0, math.Sqrt(2) / 2, math.Sqrt(2) / 2}
	result1 := YawPitchRoll(yaw, pitch, roll, point1)

	if !Close(result1, expectedResult1, 1.0E-4) {
		t.Errorf("YawPitchRoll result doesn't match case 1,\nexpected:\n%v\nGot:\n%v\n", expectedResult1, result1)
	}

	expectedResult2 := [3]float64{0.0, -math.Sqrt(2) / 2, math.Sqrt(2) / 2}
	result2 := YawPitchRoll(yaw, pitch, roll, point2)

	if !Close(result2, expectedResult2, 1.0E-4) {
		t.Errorf("YawPitchRoll result doesn't match case 2,\nexpected:\n%v\nGot:\n%v\n", expectedResult2, result2)
	}

	expectedResult3 := [3]float64{1.0, 0.0, 0.0}
	result3 := YawPitchRoll(yaw, pitch, roll, point3)

	if !Close(result3, expectedResult3, 1.0E-4) {
		t.Errorf("YawPitchRoll result doesn't match case 3,\nexpected:\n%v\nGot:\n%v\n", expectedResult3, result3)
	}

	yaw = -90.0
	pitch = -45.0
	roll = -90.0
	expectedResult4 := [3]float64{0.0, -math.Sqrt(2) / 2, -math.Sqrt(2) / 2}
	result4 := YawPitchRoll(yaw, pitch, roll, point1)

	if !Close(result4, expectedResult4, 1.0E-4) {
		t.Errorf("YawPitchRoll result doesn't match case 4,\nexpected:\n%v\nGot:\n%v\n", expectedResult4, result4)
	}

	expectedResult5 := [3]float64{0.0, math.Sqrt(2) / 2, -math.Sqrt(2) / 2}
	result5 := YawPitchRoll(yaw, pitch, roll, point2)

	if !Close(result5, expectedResult5, 1.0E-4) {
		t.Errorf("YawPitchRoll result doesn't match case 5,\nexpected:\n%v\nGot:\n%v\n", expectedResult5, result5)
	}

	expectedResult6 := [3]float64{1.0, 0.0, 0.0}
	result6 := YawPitchRoll(yaw, pitch, roll, point3)

	if !Close(result6, expectedResult6, 1.0E-4) {
		t.Errorf("YawPitchRoll result doesn't match case 6,\nexpected:\n%v\nGot:\n%v\n", expectedResult6, result6)
	}

}

func TestRotateTransPt(t *testing.T) {
	pt := [3]float64{10, 9, 8}
	angle := (float64(23) * math.Pi) / float64(180.0)
	origin := [3]float64{-1, -2, -3}
	result := [3]float64{10.0, 3.8275108, 11.423595}
	rotx := RotateX(pt, origin, angle)

	Q := GetRotationMatrix([3]float64{1, 0, 0}, angle)
	rotTrans := RotateTransPt(pt, origin, [3]float64{0.0, 0.0, 0.0}, Q)

	if !Close(rotx, rotTrans, 1.0E-4) {
		t.Errorf("RotateTransPt rotx expected:\n%#v\nGot:\n%#v\n", rotx, rotTrans)
	}

	if !Close(result, rotTrans, 1.0E-4) {
		t.Errorf("RotateTransPt result expected:\n%#v\nGot:\n%#v\n", result, rotTrans)
	}

}

func TestRotateTransPt2(t *testing.T) {
	pt := [3]float64{10, 9, 8}
	xAngle := (float64(23) * math.Pi) / float64(180.0)
	yAngle := (float64(-32) * math.Pi) / float64(180.0)
	zAngle := (float64(120) * math.Pi) / float64(180.0)
	origin := [3]float64{-1, -2, -3}

	rotxyz := RotateX(pt, origin, xAngle)
	rotxyz = RotateY(rotxyz, origin, yAngle)
	rotxyz = RotateZ(rotxyz, origin, zAngle)

	Q := GetComplexRotationMatrix(xAngle, yAngle, zAngle)
	rotTrans := RotateTransPt(pt, origin, [3]float64{0.0, 0.0, 0.0}, Q)

	if !Close(rotxyz, rotTrans, 1.0E-4) {
		t.Errorf("RotateTransPt2 rotxyz expected:\n%#v\nGot:\n%#v\n", rotxyz, rotTrans)
	}

}
