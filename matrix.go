/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com
                  Philip Godemann philip.godemann@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package vect3d

import (
	"errors"
)

// Make2D creates a slice of dimension [r][c].
// In terms of a matrix, this is analogous to an r row by c column matrix.
func Make2D(r, c int) (A [][]float64) {
	A = make([][]float64, r)

	for i := 0; i < r; i++ {
		A[i] = make([]float64, c)
	}
	return
}

// Make3D creates a slice of dimension [r][c][d].
// In terms of a matrix, this is analogous to an r row by c column by d depth matrix.
func Make3D(r, c, d int) (A [][][]float64) {
	A = make([][][]float64, r)

	for i := 0; i < r; i++ {
		A[i] = make([][]float64, c)

		for j := 0; j < c; j++ {
			A[i][j] = make([]float64, d)
		}
	}
	return
}

// Transpose2dFloat creates the transpose of a rectangular 2-dimensional slice of type float64 as if it were a transposable matrix.
func Transpose2dFloat(A [][]float64) (B [][]float64, err error) {

	for n := 1; n < len(A); n++ {
		if len(A[n-1]) != len(A[n]) {
			return nil, errors.New("cannot transpose a non-rectangular 2D slice")
		}
	}

	B = Make2D(len(A[0]), len(A))
	for i := 0; i < len(A); i++ {
		for j := 0; j < len(A[i]); j++ {
			B[j][i] = A[i][j]
		}
	}
	err = nil
	return B, err
}

// Rotate3D rotates vector b by rotation matrix A.
// This is matrix multiplication of size 3x3 X 3x1 = 3x1.
func Rotate3D(A [3][3]float64, b [3]float64) (c [3]float64) {
	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			c[i] = c[i] + A[i][j]*b[j]
		}
	}
	return c
}
