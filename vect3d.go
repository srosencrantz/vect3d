/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package vect3d

import (
	"errors"
	"math"

	"bitbucket.org/srosencrantz/vectnd"
)

// DotProduct returns the dot product of 3d vectors a and b.
func DotProduct(veca, vecb [3]float64) float64 {
	return (veca[0] * vecb[0]) + (veca[1] * vecb[1]) + (veca[2] * vecb[2])
}

// Magnitude returns the magnitude of a 3d vector.
func Magnitude(vec [3]float64) float64 {
	return math.Sqrt(DotProduct(vec, vec))
}

// CrossProduct returns the crossproduct of a and b
// a and b are 3d vectors
func CrossProduct(a, b [3]float64) (result [3]float64) {
	result[0] = a[1]*b[2] - a[2]*b[1]
	result[1] = a[2]*b[0] - a[0]*b[2]
	result[2] = a[0]*b[1] - a[1]*b[0]
	return result
}

// GetAngle returns cos(theta) where theta is the angle between 3d vectors a and b.
func GetAngle(a, b [3]float64) float64 {
	return DotProduct(a, b) / (Magnitude(a) * Magnitude(b))
}

// GetSignedAngle returns the signed angle theta (radians) between 3d vectors a and b about the given the axis.
func GetSignedAngle(a, b, axis [3]float64) float64 {
	angle := math.Acos(GetAngle(a, b))
	cross := CrossProduct(a, b)
	if DotProduct(axis, cross) < 0 { // Or > 0
		angle = -angle
	}
	return angle
}

// Subtract returns the 3d vector a - b
func Subtract(a, b [3]float64) (result [3]float64) {
	result[0] = a[0] - b[0]
	result[1] = a[1] - b[1]
	result[2] = a[2] - b[2]
	return result
}

// Add returns the 3d vector a + b
func Add(a, b [3]float64) (result [3]float64) {
	result[0] = a[0] + b[0]
	result[1] = a[1] + b[1]
	result[2] = a[2] + b[2]
	return result
}

// ScalerMult returns a * b
// a is three dimensional vector
// b is a scalar
func ScalerMult(a [3]float64, b float64) (result [3]float64) {
	result[0] = a[0] * b
	result[1] = a[1] * b
	result[2] = a[2] * b
	return result
}

// Normalize returns a/|a| where a is a 3d vector
func Normalize(a [3]float64) [3]float64 {
	return ScalerMult(a, float64(1)/Magnitude(a))
}

// TriNormal returns the normal of a triangle with a normal that is the cross product of n2 - n1 and n3 - n2
func TriNormal(n1, n2, n3 [3]float64) [3]float64 {
	return CrossProduct(Subtract(n2, n1), Subtract(n3, n2))
}

// NearestPtOnLine returns the point on a line defined by point a and normalized vector n
// that is closest to b
// a is a point on the line
// n is the NORMALIZED vector of the line
// b is a point not necessarily on the line
func NearestPtOnLine(a, n, b [3]float64) [3]float64 {
	return Add(a, ScalerMult(n, DotProduct(Subtract(b, a), n)))
}

// DistPt2Line returns the shortest distance from b to the line defined by a and n
// a is a point on the line
// n is the vector of the line
// b is a point not on the line
// The result is the shortest distance from b to the line
func DistPt2Line(a, n, b [3]float64) float64 {
	subAB := Subtract(a, b)
	normN := Normalize(n)
	return Magnitude(Subtract(subAB, ScalerMult(normN, DotProduct(subAB, normN))))
}

// DistPt2Pt returns the distance from 3d point a to 3d point b
// a is a point
// b is another point
// The result is the shortest distance from b to the line
func DistPt2Pt(a, b [3]float64) float64 {
	return Magnitude(Subtract(a, b))
}

// historical from java_geometry_lib
// public Vector3d getANormVectAtPoint(double x0, double y0, double z0){
//   if((x == 0)||(y == 0)||(z == 0)){
//     if(x==0){
//       return new Vector3d(x0 + 1, y0, z0);
//     }
//     if(y==0){
//       return new Vector3d(x0, y0 + 1, z0);
//     }
//     if(z==0){
//       return new Vector3d(x0, y0, z0 + 1);
//     }
//   }
//   return new Vector3d(x0 + 1, y0 - (x/y), z0);
// }
// public double angle(Vector3d vec2) {
//     double length = length();
//     double length2 = vec2.length();
//     if ((length > 0) && (length2 > 0)) {
//       return Math.acos(dot(vec2) / (length * length2));
//     }
//     return 0;
//   }

// Translate moves a 3d point a along the vector dir to 3d point b
// a is the point to be moved
// b is the moved point
// dir is a vector representing the direction and distance a is moved.
func Translate(a, dir [3]float64) (b [3]float64) {
	return Add(a, dir)
}

// TranslateDist moves a 3d point a along the vector dir a distance d to 3d point b
// a is the point to be moved
// b is the moved point
// dir is a vector representing the direction and distance a is moved.
func TranslateDist(a, dir [3]float64, dist float64) (b [3]float64) {
	return Translate(a, ScalerMult(Normalize(dir), dist))
}

// ReflectX moves a point to its mirrored location about the X plane
// a is the point to be moved
// b is the moved point
func ReflectX(a [3]float64) (b [3]float64) {
	return Translate(a, [3]float64{float64(-2.0) * a[0], 0, 0})
}

// ReflectY moves a point to its mirrored location about the X plane
// a is the point to be moved
// b is the moved point
func ReflectY(a [3]float64) (b [3]float64) {
	return Translate(a, [3]float64{0, float64(-2.0) * a[1], 0})
}

// ReflectZ moves a point to its mirrored location about the X plane
// a is the point to be moved
// b is the moved point
func ReflectZ(a [3]float64) (b [3]float64) {
	return Translate(a, [3]float64{0, 0, float64(-2.0) * a[2]})
}

// RotateX rotates point a about the given origin and X axis a specified angle (radians)
// a is the point to be moved
// b is the moved point
func RotateX(a, origin [3]float64, angle float64) (b [3]float64) {
	if math.Abs(angle) < math.SmallestNonzeroFloat64 {
		return a
	}
	return RotateAxis(a, origin, [3]float64{1.0, 0.0, 0.0}, angle)
}

// RotateY rotates point a about the given origin and Y axis a specified angle (radians)
// a is the point to be moved
// b is the moved point
func RotateY(a, origin [3]float64, angle float64) (b [3]float64) {
	if math.Abs(angle) < math.SmallestNonzeroFloat64 {
		return a
	}
	return RotateAxis(a, origin, [3]float64{0.0, 1.0, 0.0}, angle)
}

// RotateZ rotates point a about the given origin and Z axis a specified angle (radians)
// a is the point to be moved
// b is the moved point
func RotateZ(a, origin [3]float64, angle float64) (b [3]float64) {
	if math.Abs(angle) < math.SmallestNonzeroFloat64 {
		return a
	}
	return RotateAxis(a, origin, [3]float64{0.0, 0.0, 1.0}, angle)
}

// RotateN1N2 rotates a point a about an origin and an axis, formed by n2-n1, a specified angle (radians)
// a is the point to be moved
// b is the moved point
func RotateN1N2(a, origin, n1, n2 [3]float64, angle float64) (b [3]float64) {
	if math.Abs(angle) < math.SmallestNonzeroFloat64 {
		return a
	}
	axis := Subtract(n2, n1)
	return RotateAxis(a, origin, axis, angle)
}

// RotateAxis rotates a point a about an origin and an axis a specified angle (radians)
// a is the point to be moved
// b is the moved point
// It doesn't require the axis to be normalized.
func RotateAxis(a, origin, axis [3]float64, angle float64) (b [3]float64) {
	q := ScalerMult(Normalize(axis), math.Sin(angle/float64(2.0)))
	q0 := math.Cos(angle / float64(2.0))

	u := Subtract(a, origin)

	q0s := q0 * q0
	qv0s := q[0] * q[0]
	qv1s := q[1] * q[1]
	qv2s := q[2] * q[2]

	qv0qv1 := q[0] * q[1]
	qv1qv2 := q[1] * q[2]
	qv0qv2 := q[0] * q[2]

	q0qv2 := q0 * q[2]
	q0qv1 := q0 * q[1]
	q0qv0 := q0 * q[0]

	Q11 := q0s + qv0s - qv1s - qv2s
	Q12 := 2.0 * (qv0qv1 - q0qv2)
	Q13 := 2.0 * (qv0qv2 + q0qv1)
	Q21 := 2.0 * (qv0qv1 + q0qv2)
	Q22 := q0s - qv0s + qv1s - qv2s
	Q23 := 2.0 * (qv1qv2 - q0qv0)
	Q31 := 2.0 * (qv0qv2 - q0qv1)
	Q32 := 2.0 * (qv1qv2 + q0qv0)
	Q33 := q0s - qv0s - qv1s + qv2s
	b[0] = u[0]*Q11 + u[1]*Q12 + u[2]*Q13 + origin[0]
	b[1] = u[0]*Q21 + u[1]*Q22 + u[2]*Q23 + origin[1]
	b[2] = u[0]*Q31 + u[1]*Q32 + u[2]*Q33 + origin[2]
	return b
}

// GetRotationMatrix returns the Q matrix needed for RotateTransPt
func GetRotationMatrix(axis [3]float64, angle float64) (Q [][]float64) {
	Q = Make2D(3, 3)
	q := ScalerMult(Normalize(axis), math.Sin(angle/float64(2.0)))
	q0 := math.Cos(angle / float64(2.0))

	Q[0][0] = (q0 * q0) + (q[0] * q[0]) - (q[1] * q[1]) - (q[2] * q[2])
	Q[0][1] = 2.0 * ((q[0] * q[1]) - (q0 * q[2]))
	Q[0][2] = 2.0 * ((q[0] * q[2]) + (q0 * q[1]))
	Q[1][0] = 2.0 * ((q[0] * q[1]) + (q0 * q[2]))
	Q[1][1] = (q0 * q0) - (q[0] * q[0]) + (q[1] * q[1]) - (q[2] * q[2])
	Q[1][2] = 2.0 * ((q[1] * q[2]) - (q0 * q[0]))
	Q[2][0] = 2.0 * ((q[0] * q[2]) - (q0 * q[1]))
	Q[2][1] = 2.0 * ((q[1] * q[2]) + (q0 * q[0]))
	Q[2][2] = (q0 * q0) - (q[0] * q[0]) - (q[1] * q[1]) + (q[2] * q[2])

	return Q
}

// GetComplexRotationMatrix returns a compound rotation matrix for rotating about the x, y, and z axis respectively.
func GetComplexRotationMatrix(xAngle, yAngle, zAngle float64) (Q [][]float64) {
	qX := GetRotationMatrix([3]float64{1, 0, 0}, xAngle)
	qY := GetRotationMatrix([3]float64{0, 1, 0}, yAngle)
	qZ := GetRotationMatrix([3]float64{0, 0, 1}, zAngle)
	Q = vectnd.Mult2D(vectnd.Mult2D(qZ, qY), qX)
	return Q
}

// RotateTransPt rotates a point given a rotation matrix Q, and an origin and translates it by trans
func RotateTransPt(a, origin, trans [3]float64, Q [][]float64) (b [3]float64) {
	u := Subtract(a, origin)
	b[0] = u[0]*Q[0][0] + u[1]*Q[0][1] + u[2]*Q[0][2] + origin[0] + trans[0]
	b[1] = u[0]*Q[1][0] + u[1]*Q[1][1] + u[2]*Q[1][2] + origin[1] + trans[1]
	b[2] = u[0]*Q[2][0] + u[1]*Q[2][1] + u[2]*Q[2][2] + origin[2] + trans[2]
	return b
}

// RotateN1N2N3 rotates a point around an origin in the plane specified by three points in the plane (n1, n2, and n3)
// a is the point to be moved
// b is the moved point
func RotateN1N2N3(a, origin, n1, n2, n3 [3]float64, angle float64) (b [3]float64, err error) {
	if math.Abs(angle) < math.SmallestNonzeroFloat64 {
		return a, nil
	}
	vec1 := Subtract(n2, n1)
	vec2 := Subtract(n3, n2)

	// if math.Abs(angle) < 0.00001 {
	// 	return
	// }

	planeVecAngle := math.Abs(GetAngle(vec1, vec2))

	if (planeVecAngle < 0.00001) || ((planeVecAngle < 180.00001) && (planeVecAngle > 179.99999)) {
		return a, errors.New("n1, n2, n3 are colinear")
	}

	// 	temp := Add(n1, [3]float64{1.0, 0, 0})
	// 	a = Subtract(n2, temp)
	// 	planeVecAngle = math.Abs(GetAngle(vec1, vec2))
	// 	if (planeVecAngle < 0.00001) || ((planeVecAngle < 180.00001) && (planeVecAngle > 179.99999)) {
	// 		temp = Add(n1, [3]float64{0, 1.0, 0})
	// 		a = Subtract(n2, temp)
	// 	}
	// }

	axisOfRot := CrossProduct(vec2, vec1)

	return RotateAxis(a, origin, axisOfRot, angle), nil
}

// YawPitchRoll rotates a point through yaw, pitch, and roll, in that order according to the Tait-Bryan z-y'-x'' intrinsic rotation convention.
// yaw, pitch, and roll are entered in degrees with bounds: -360 < yaw < 360; -360 < pitch < 360; -360 < roll < 360.
// pt is the point to be rotated.
// rotpt is the rotated point.
func YawPitchRoll(yaw, pitch, roll float64, pt [3]float64) (rotpt [3]float64) {
	const toRad = math.Pi / 180.0
	const halfPi = math.Pi / 2.0
	yaw *= toRad
	pitch *= -toRad
	roll *= toRad

	c1 := math.Sin(halfPi - yaw)
	c2 := math.Sin(halfPi - pitch)
	c3 := math.Sin(halfPi - roll)
	s1 := math.Sin(yaw)
	s2 := math.Sin(pitch)
	s3 := math.Sin(roll)

	R := [3][3]float64{[3]float64{c1 * c2, c1*s2*s3 - c3*s1, s1*s3 + c1*c3*s2},
		[3]float64{c2 * s1, c1*c3 + s1*s2*s3, c3*s1*s2 - c1*s3},
		[3]float64{-s2, c2 * s3, c2 * c3}}

	rotpt = Rotate3D(R, pt)

	return rotpt
}
