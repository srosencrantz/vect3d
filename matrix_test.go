/* Copyright 2016 Stephen Rosencrantz srosencrantz@gmail.com
                  Philip Godemann philip.godemann@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package vect3d

import (
	"testing"
)

func TestMake2D(t *testing.T) {
	r := 3
	c := 4
	A := Make2D(r, c)

	if rows := len(A); rows != r {
		t.Errorf("Make2D row error. Expected %d got %d.", r, rows)
	}

	for i := 0; i < len(A); i++ {
		if cols := len(A[i]); cols != c {
			t.Errorf("Make2D column error. Expected %d got %d.", c, cols)
		}
	}
}

func TestMake3D(t *testing.T) {
	r := 3
	c := 4
	d := 5
	A := Make3D(r, c, d)

	if rows := len(A); rows != r {
		t.Errorf("Make3D row error. Expected %d got %d.", r, rows)
	}

	for i := 0; i < len(A); i++ {
		if cols := len(A[i]); cols != c {
			t.Errorf("Make3D column error. Expected %d got %d.", c, cols)
		}

		for j := 0; j < len(A[i]); j++ {
			if depth := len(A[i][j]); depth != d {
				t.Errorf("Make3D depth error. Expected %d got %d.", d, depth)
			}
		}
	}
}

func TestTranspose2dFloat(t *testing.T) {
	input := [][]float64{
		[]float64{1, 2, 3},
		[]float64{4, 5, 6},
		[]float64{7, 8, 9},
	}

	expectedOutput := [][]float64{
		[]float64{1, 4, 7},
		[]float64{2, 5, 8},
		[]float64{3, 6, 9},
	}

	B, _ := Transpose2dFloat(input)
	for i := 0; i < len(B); i++ {
		for j := 0; j < len(B[i]); j++ {
			if B[i][j] != expectedOutput[i][j] {
				t.Errorf("Transpose2dFloat error. The function did not produce the expected output.")
			}
		}
	}
}

func TestRotate3D(t *testing.T) {
	I := [3][3]float64{[3]float64{1, 0, 0}, [3]float64{0, 1, 0}, [3]float64{0, 0, 1}}
	input := [3]float64{1, 2, 3}

	output := Rotate3D(I, input)
	if output != input {
		t.Errorf("Rotate3D error. Multiplication by the identity matrix did not yeild the original vector. Expected %#v got %#v.", input, output)
	}

	ones := [3][3]float64{[3]float64{1, 1, 1}, [3]float64{1, 1, 1}, [3]float64{1, 1, 1}}
	output1 := Rotate3D(ones, input)
	expectedOutput := [3]float64{6, 6, 6}
	if output1 != expectedOutput {
		t.Errorf("Rotate3D error. Multiplication by the ones matrix did not yeild the expected vector. Expected %#v got %#v.", expectedOutput, output1)
	}

}
